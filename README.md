# jnk_cutter

A simple bash script for splitting a video file using [ffmpeg](https://ffmpeg.org) into sections, saving each section in an individual file, and also extracting audio from each video into a separate "mp3" (320kbps bitrate is enforced).

Note: the script splits the video and transcodes the sections to 1280x720p (with the original audio preserved). You can override this behaviour by using `--no-transcode` flag.

By default, the script skips every other section starting from the first one. You can override this behaviour by using `--no-skip` flag. For example if you pass 2 break times (with `-t` flag) to the script, sections 1 and 3 will be skipped as shown below:

```text
time:     0-----------------t-----------------t-----------------(end)
sections: |----section 1----|----section 2----|----section 3----|
```

## Installation

`jnk_cutter` is a simple script and just needs the following tools to work:

* bash
* ffmpeg
* bc (optional)

You should be able to run the script in any environment with `bash` and `ffmpeg` support. We test the script on Ubuntu 22.04 and Cygwin 3.4.

## Usage

* See help

`./jnk_cutter.sh -h`

* Just validate the input flags and print the ffmpeg commands but do not execute them

`./jnk_cutter.sh --dry-run`

* Cut input.mp4 into a single section: form the 1-min mark to the end. The output files will be named: input_1.mp3, input_1.mp4

`./jnk_cutter.sh -i input.mp4 -t 00:01:00`

* Cut input.mp4 into a single section: form the 1-min mark to the end (similar to the example above). Depending on the frequency of key frames in the original video, a few seconds at the start of output video may appear frozen/corrupted!

`./jnk_cutter.sh -i input.mp4 -t 00:01:00 --no-transcode`

* Cut input.mp4 into two sections: from the start to the 1-min mark and form the 1-min mark to the end. The output files will be named: input_1.mp3, input_1.mp4, input_2.mp3, input_2.mp4

`./jnk_cutter.sh -i input.mp4 -t 00:01:00 --no-skip`

* Same as the above but change the output file names to output_*.mp3/mp4. You can define the prefix with a different file extension if you want to change the data container e.g. `--output-prefix=output.mkv`

`./jnk_cutter.sh -i input.mp4 --output-prefix=output -t 00:01:00 --no-skip`

* Same as the above but change the output file names to file1.mp3/mp4 & file2.mp3/mp4. (File names which include space or single quotation, must be enclosed in double quotation marks as: `--output-files="file 1,file's2"`) You can define the filenames with a different extension if you want to change the data container e.g. `--output-files=file1.mkv,file2.mp4`

`./jnk_cutter.sh -i input.mp4 --output-files=file1,file2 -t 00:01:00 --no-skip`

* Split the files by defining multiple break points. (timing of the output files: start -> 01:00, 01:00 -> 02:00, 02:00 -> 03:00, 03:00 -> end)

`./jnk_cutter.sh -i input.mp4 -t 00:01:00 -t 00:02:00 -t 00:03:00 --no-skip`

* Split the files by defining multiple break points skipping every other section. (timing of the output files: 01:00 -> 02:00, 03:00 -> end)

`./jnk_cutter.sh -i input.mp4 -t 00:01:00 -t 00:02:00 -t 00:03:00`

* Split the files by defining multiple break points skipping only the sections 1 & 4. (timing of the output files: 01:00 -> 02:00, 02:00 -> 03:00)

`./jnk_cutter.sh -i input.mp4 -t 00:01:00 -t 00:02:00 -t 00:03:00 --skip=1,4`

## Time Formats

Here’s a few example of valid time formats:

* `12` (will be interpreted as 00h 00m 12s)
* `12.3` (will be interpreted as 00h 00m 12s 300ms)
* `1:23` (will be interpreted as 00h 01m 23s)
* `1:2:3` (will be interpreted as 01h 02m 03s)

See [ffmpeg documentation](https://ffmpeg.org/ffmpeg-utils.html#Time-duration) for details on the time definition formats. `jnk_cutter` currently does not support the ffmpeg time duration formats which include `s|ms|us` suffixes and/or negative durations `-`.

## License

© 2021-2022 Jnktn.tv distributed under the terms of BSD-3-Clause (see LICENSE)

CLI parser generated using [Argbash v2.9.0](https://github.com/matejak/argbash) (BSD-3-Clause)

See [git commits history](https://codeberg.org/jnktn/jnk_cutter/commits/branch/main) for the complete list of collaborators.
