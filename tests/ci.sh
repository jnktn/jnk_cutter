# Usage:
#        expect_duration [filename] [expected duration in seconds]
expect_duration () {

  _color_red="\033[31;49;1m"
  _color_green="\033[32;49;1m"
  _color_reset="\033[0m"

  _filename=$1
  _file_base=${_filename%.*}
  _file_ext=${_filename#"$_file_base".}


  if [ ! -f "${_filename}" ]; then
    echo "${_color_red}> ERROR: ${_filename} could not be found!${_color_reset}"
    return 1;
  fi

  if [ "${_file_ext}" = "mkv" ]; then
    _duration=$(ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 "${_filename}")
  elif [ "${_file_ext}" = "mp4" ] || [ "${_file_ext}" = "mp3" ]; then
    _duration=$(ffprobe -v error -select_streams a:0 -show_entries stream=duration -of default=noprint_wrappers=1:nokey=1 "${_filename}")
  else
    echo "${_color_red}> ERROR: Unrecognized file format! Duration of this file format could not be estimated.${_color_reset}"
    return 1;
  fi

  _duration_reference=$2
  _comparison_tol="0.5"
  _duration_diff=$(echo "${_duration}-${_duration_reference}" | bc -l)


  echo "Filename: ${_filename}"
  echo "Actual duration   = ${_duration} sec"
  echo "Expected duration = ${_duration_reference} sec"
  echo "Comparison tolerance = ${_comparison_tol} sec"
  echo "Duration difference = ${_duration_diff} sec"

  if [ "$(echo "${_duration_diff}>-${_comparison_tol}" | bc -l)" -ne 0 ]  && [ "$(echo "${_duration_diff}<${_comparison_tol}" | bc -l)" -ne 0 ]; then 
    echo "${_color_green}> PASS: Actual duration matches the expected duration.${_color_reset}"
    return 0;
  else
    echo "${_color_red}> FAIL: Actual duration DOES NOT match the expected duration!${_color_reset}"
    return 1;
  fi;
}

# Usage:
#        expect_audio_bitrate [filename] [expected bitrate in bps]
expect_audio_bitrate() {

  _color_red="\033[31;49;1m"
  _color_green="\033[32;49;1m"
  _color_reset="\033[0m"

  _filename=$1
  _file_base=${_filename%.*}
  _file_ext=${_filename#"$_file_base".}


  if [ ! -f "${_filename}" ]; then
    echo "${_color_red}> ERROR: ${_filename} could not be found!${_color_reset}"
    return 1;
  fi

  if [ "${_file_ext}" = "mp3" ]; then
    _bitrate=$(ffprobe -v error -select_streams a:0 -show_entries stream=bit_rate -of default=noprint_wrappers=1:nokey=1 "${_filename}")
  else
    echo "${_color_red}> ERROR: Unrecognized file format! Bitrate of this file format could not be estimated.${_color_reset}"
    return 1;
  fi

  _bitrate_reference=$2
  _comparison_tol="10"
  _bitrate_diff=$(echo "${_bitrate}-${_bitrate_reference}" | bc -l)


  echo "Filename: ${_filename}"
  echo "Actual bitrate   = ${_bitrate} bps"
  echo "Expected bitrate = ${_bitrate_reference} bps"
  echo "Comparison tolerance = ${_comparison_tol} bps"
  echo "Duration difference = ${_bitrate_diff} bps"

  if [ "$(echo "${_bitrate_diff}>-${_comparison_tol}" | bc -l)" -ne 0 ]  && [ "$(echo "${_bitrate_diff}<${_comparison_tol}" | bc -l)" -ne 0 ]; then 
    echo "${_color_green}> PASS: Actual bitrate matches the expected bitrate.${_color_reset}"
    return 0;
  else
    echo "${_color_red}> FAIL: Actual bitrate DOES NOT match the expected bitrate!${_color_reset}"
    return 1;
  fi;

}


# Usage:
#        expect_file_count [path] [expected number of files]

expect_file_count() {

  _color_red="\033[31;49;1m"
  _color_green="\033[32;49;1m"
  _color_reset="\033[0m"

  _expected_count=$2

  files_count="$(find "$1" -type f | wc -l)"
  if [ "$files_count" -eq "$_expected_count" ]; then
    echo "${_color_green}> PASS: Found all the expected files.${_color_reset}";
    return 0;
  elif [ "$files_count" -lt "$_expected_count" ]; then
    echo "${_color_red}> FAIL: Could not find all the files in the provided path!"
  else
    echo "${_color_red}> FAIL: Extra file was enocuntered in the provided path!"
  fi

  echo ">> PATH: $1"
  echo ">> EXPECTED: $_expected_count files"
  echo ">> DISCOVERED: $files_count files${_color_reset}"
  command ls -la "$1"
  return 1;
}